import './App.css';
import { useState } from 'react';
import {useParams, Linkn, useNavigate} from "react-router-dom";
import Card from './Card';



function App() {
  let navigate = useNavigate()
 
  const [number, setNumber] = useState('')
  const [nameC, setName] = useState('')
  const [expiry, setExpiry] = useState('')
  const [cvc, setCvc] = useState('')
  function handleClick() {
    navigate('/Card',{
      state: {
        num: number,
        expdate: expiry,
        cvcc: cvc,
      }
      
    });
    
  } 

  return (

    <div class="card">
     <form name="form"> 
      <div class="input">
       <input type='tel' name='number' placeholder='Card Number' value={number} onChange={e => setNumber(e.target.value)} class="card__input"/>
       </div>

       <div  class="input">
       <input type='text' name='nameC' placeholder='Card Name' value={nameC} onChange={e => setName(e.target.value)} class="card__input"/>
       </div>

       <div  class="input">
       <input type='text' name='expiry' placeholder='Card expiry' value={expiry} onChange={e => setExpiry(e.target.value)} class="card__input"/>
       </div>
        <div  class="input">
       <input type='tel' name='CVC' placeholder='Card CVC' value={cvc} onChange={e => setCvc(e.target.value)} class="card__input"/>
       </div>

       <div class="container">
       <button class="btn"  onClick={handleClick}> Submit </button>
        </div>
     </form>
    </div>
   
   
  );
}

export default App;
