import React, { Component } from 'react';
import { useNavigate, useLocation } from "react-router-dom";
import App from './App';


function Card ()  {
  const {state} = useLocation();
  let navigate = useNavigate()
  function handleClick() {
    navigate('/')
  } 
    return (
        <div>
                   <button class="btn" onClick={handleClick}>BACK</button>
         <div class="card">
      <form>
        <h1 class="card__title"> Payment Information</h1>
        <div class="card__row">
          <div class="card__col">
            <label for="cardNumber" class="card__label">Card Number</label
            ><input
              type="text"
              class="card__input card__input--large"
              id="cardNumber"
              value={state.num}
            />
          </div>
          <div class="card__col card__chip">
            <img src="img/chip.svg" alt="chip" />
          </div>
        </div>
        <div class="card__row">
          <div class="card__col">
            <label for="cardExpiry" class="card__label">Expiry Date</label
            ><input
              type="text"
              class="card__input"
              id="cardExpiry"
              value={state.expdate}
            />
          </div>
          <div class="card__col">
            <label for="cardCcv" class="card__label">CCV</label
            ><input
              type="text"
              class="card__input"
              id="cardCcv"
              value={state.cvcc}
            />
          </div>
          <div class="card__col card__brand"><i id="cardBrand"></i></div>
        </div>
        
      </form>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/cleave.js@1.6.0/dist/cleave.min.js"></script> 
       
       
        </div>
      );
}
export default Card;
